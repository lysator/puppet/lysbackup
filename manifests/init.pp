# Lysbackup
class lysbackup (
  String $backup_user,
  String $user = 'root',
  Optional[Array[String]] $exclude = [],
) {

  package { 'borgbackup':
    ensure => 'installed',
  }

  file { '/opt/lysbackup':
    ensure  => directory,
    recurse => true,
    mode    => '0744',
    source  => 'puppet:///modules/lysbackup/opt/lysbackup/',
  } -> file { '/opt/lysbackup/puppet_exclude.txt':
    ensure  => file,
    # << '' for trailing newline
    content => join($exclude << '', "\n"),
  }

  cron { 'borgbackup':
    ensure  => present,
    command => "/usr/bin/python3 /opt/lysbackup/backup.py ${backup_user}-backup",
    user    => $user,
    minute  => '05',
    hour    => '04',
  }
}
